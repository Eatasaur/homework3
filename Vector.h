#include <iostream>
#include <exception>
using namespace std;
//Uhm.. I'm not very sure how to work with the exception.. So, could you please go into more detail about it this Tuesday? Thanks!
class Vector
{
public:
	Vector(int n);
	Vector(const Vector &v);
	Vector();
	int getSize();
	int getCapacity();
	bool empty();
	void assign(int val);
	void clear();
	void push_back(const int& val);
	void pop_back();
	void reserve( int n);
	void resize( int n);
	void resize( int n, const int& val);
	int operator[](int n);
	~Vector();

private:
	int* _begin;
	int _size;
	int _capacity;
	int _resizeFactor;
};