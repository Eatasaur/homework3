#include "Vector.h"

Vector:: Vector(int n):_size(n),_resizeFactor(n)
{
	_begin = new int[_size];
}

Vector:: Vector(const Vector &v)
{
	int i;
	this->_size = v._size;
	this->_capacity = v._capacity;
	this->_resizeFactor = v._resizeFactor;
	this->_begin = new int[this->_size];
	for(i=0;i<_size;i++)
	{
		*(this->_begin+i) = *(v._begin+i);
	}
}

Vector:: Vector():_size(10),_resizeFactor(10)
{
	_begin = new int[_size];
}

int Vector:: getSize()
{
	return this->_size;
}

int Vector:: getCapacity()
{
	return this->_capacity;
}

bool Vector:: empty()
{
	if(this->_size == 0)
	{
		return true;
	}
	else return false;
}

void Vector:: assign(int val)
{
	int i;
	for(i = 0 ; i < this->_size ;i++)
	{
		*(this->_begin + i) = val;
	}
}

void Vector:: clear()
{
	delete[] this->_begin;
}

void Vector:: push_back(const int& val)
{
	if(this->_size == this->_capacity)
	{
		int i;
		int* temp = _begin;
		int* bigVector = new int[_size+_resizeFactor];
		for(i = 0; i < _size;i++)
		{
			*(bigVector +i) = *(_begin + i);
		}
		_begin = bigVector;
		delete[] temp;
	}
	*(_begin+(++_size)) = val;
}

void Vector:: pop_back()
{
	if(this->_size == 0)
	{
		return;
	}
	*(_begin+(--_size)) = 0;
}

void Vector:: reserve( int n)
{
	if(this->_capacity < n)
	{
		int i;
		int* temp = _begin;
		int* bigVector = new int[_size+_resizeFactor];
		for(i = 0; i < _size;i++)
		{
			*(bigVector +i) = *(_begin + i);
		}
		_begin = bigVector;
		delete[] temp;
	}
}

void Vector:: resize(int n)
{
	int i;
	if(this->_size > n)
	{
		for(i=n+1;i<this->_size;i++)
		{
			*(this->_begin+i) = 0;
		}
	}
	if(this->_size < n)
	{
		//I'm not sure what values to add, so I guess I'll add 1's.
		for(i=this->_size;i<n;i++)
		{
			push_back(1);
		}
	}
	//The reason I didn't check the capacity is because in both ways, pushback would do the same thing. I mean, if n is bigger than the capacity, then push_back will resize the vector. I'm probably wrong on this one, but... Oh well?
}

void Vector:: resize(int n, const int& val)
{
	int i;
	if(this->_size > n)
	{
		for(i=n+1;i<this->_size;i++)
		{
			*(this->_begin+i) = 0;
		}
	}
	if(this->_size < n)
	{
		for(i=this->_size;i<n;i++)
		{
			push_back(val);
		}
	}
}

int Vector:: operator[](int n)
{
	return *(this->_begin+n);
}

Vector::~Vector()
{
	delete[] this->_begin;
}
